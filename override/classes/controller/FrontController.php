<?php

class FrontController extends FrontControllerCore
{

    public function init()
    {
        if ((!$this->context->customer->isLogged() && $this->php_self != 'authentication' && $this->php_self != 'password')) {
            Tools::redirect('index.php?controller=authentication');
        }
        parent::init();
    }

    public function getLayout()
    {
        if ($this->php_self === 'authentication' || $this->php_self === 'password') {
            $layout = 'layouts/layout-content-only.tpl';
        } else {
            $layout = parent::getLayout();
        }

        return $layout;
    }

}
